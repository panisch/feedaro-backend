'''
Created on 10.06.2012

@author: dominik
'''
from feedaro.misc import config
from nltk.corpus.reader.bracket_parse import BracketParseCorpusReader
import nltk
class PosTagger(object):
    def __init__(self):
        corpus_root=config.get("data","tigercorpus")
        file_pattern = r".*\.penn"
        cr = BracketParseCorpusReader(corpus_root, file_pattern)
        #Number of most frequent words in Tiger corpus, applied as corpus model
       # numMostFrequentWords=15000
        fd=nltk.FreqDist(cr.words())
        cfd=nltk.ConditionalFreqDist(cr.tagged_words())
        most_freq_words=fd.keys()[:10000]
        
        likely_tags=dict((word,cfd[word].max()) for word in most_freq_words)
       # tagged_sents=cr.tagged_sents()[:numMostFrequentWords]
        
        #tagger = nltk.tag.UnigramTagger(train=tagged_sents, backoff=None) 
        self.tagger = nltk.tag.UnigramTagger(model=likely_tags, backoff=None) 
      
    def tag(self,words):
        erg = self.tagger.tag(words)
        lower_words =[b[0].lower() for b in erg if b!=[] and (b[1]=='NN-NK' or b[1]==None or (b[1] !=None and b[1].startswith('VV')))]
        return lower_words