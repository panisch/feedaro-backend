'''
Created on 15.05.2012

@author: dominik
'''

from ConfigParser import SafeConfigParser
import sys,os

def get(section,prop):
    parser = SafeConfigParser()
    f= os.path.join(os.path.dirname(os.path.abspath(__file__)), '../config.ini')
    parser.read(f)
    return parser.get(section,prop)

LOG_CONF = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '[%(asctime)-12s] [%(levelname)s] %(name)s: %(message)s', 
            'datefmt': '%b %d %H:%M:%S'
        },
    },
    'handlers': {
        'console':{
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
        'log_file':{
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'feedaro/logs/feedaro.log',
            'maxBytes': '16777216', # 16megabytes
            'formatter': 'verbose'
        },
    },
    'loggers': {
        '': {
            'handlers': ['console', 'log_file'],
            'level': 'DEBUG',
            'propagate': True
        },
    }
}
