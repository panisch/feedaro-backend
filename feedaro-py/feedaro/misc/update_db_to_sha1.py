from feedaro.db import db_tools 
import hashlib
db = db_tools.get_db()
feeds = db.feeds.find()

for feed in feeds:
    new_hash=[]
    for i,hash in enumerate(feed['post_hashes']):
        post = db.posts.find_one({'hash':hash})
        post['hash']=hashlib.sha1(post['link']).hexdigest()
        new_hash.append(post['hash'])
    feed['post_hashes']=new_hash
    db.feeds.update({"_id":feed["_id"]},feed)


        