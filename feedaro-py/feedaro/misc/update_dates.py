'''
Created on Jun 24, 2012

@author: dominik
'''
from feedaro.db import db_tools
from bson.objectid import ObjectId
for post in db_tools.get_posts().find():
    time = post["_id"].generation_time
    db_tools.get_posts().update({"_id" :post['_id']}, {'$set': {"date":time}})
