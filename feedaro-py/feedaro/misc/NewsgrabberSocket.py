'''
Created on 15.05.2012

@author: dominik
'''
import socket

IP="127.0.0.1"
PORT=6669
def getText(url):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((IP,PORT))
    rfile = s.makefile('rb', -1)
    wfile = s.makefile('wb', 0)
    wfile.write(url+"\n")
    answer= rfile.read()
    return answer.decode('utf-8')