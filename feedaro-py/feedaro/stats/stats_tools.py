'''
Created on 18.06.2012

@author: Patrick
'''
import redis
import logging
from logging.config import dictConfig
import operator
from BitVector import BitVector
from feedaro.db import db_tools
from feedaro.misc import config

SIMILARITY_BITMAP_SIZE = 22000
USER_SIZE=30

def init_redis():
    set_redis(1)
    set_redis(0)
    
def set_redis(value=0):
    db = get_redis()
    ids=range(USER_SIZE)
    hashes=range(SIMILARITY_BITMAP_SIZE)
    for id in ids:
        for hash in hashes:
            db.setbit("userid:%s"%id,hash,value)
            
def get_redis():
    db = redis.Redis(config.get("redisdb","url"))
    return db

def add_post_user(user_id, post_seqno):
    redis_db = get_redis()
    #storing post views
    key = "storyid:%s"% post_seqno
    redis_db.setbit(key, user_id, 1)
    views= hemmingweight(key)
    db_tools.addvote2post(post_seqno, views)
    #storing user readings
    logging.debug("Add post for user %s" %(user_id))
    redis_db.setbit("userid:%s"% user_id, post_seqno, 1)
    
def count_user_readings(user_id):
    db = get_redis()
    return db.scard("posts:reading:userid:%s" % user_id)

def hemmingweight(bitkey):
    '''returns the hemming weight for the giben bitkey in redis
        hemming weight: counts all 1's in binary representation'''    
    db = get_redis()
    a = bin(int(byte2hex(db.get(bitkey)),16))[2:]
    return a.count('1')

def byte2hex(byteStr):
    return ''.join( [ "%02X " % ord( x ) for x in byteStr ] ).strip("").replace(" ","")

def get_bitmap(userid):
    db= get_redis()
    user_values = db.get("userid:%s"% userid)
    if user_values is None:
        logging.debug("No stats for userid %s" %userid)
        return None
    size= db_tools.get_posts_count()
    return BitVector(intVal = int(byte2hex(user_values),16), size=SIMILARITY_BITMAP_SIZE)

def get_similar_users(userid, user_ids):
    sim={}
    user_bitmap = get_bitmap(userid)
    if not user_bitmap:
        return sim 
    for id in user_ids:
        if id ==userid:
            continue
        other_bitmap = get_bitmap(id)
        if other_bitmap is None:
            continue
        similarity = user_bitmap.jaccard_similarity(other_bitmap)
        logging.debug("Similarity from userid %s to userid %s is: %f" %(userid,id,similarity))
        if similarity > 0.1:
            sim[id] = similarity
    sorted_sim = sorted(sim.iteritems(), key=operator.itemgetter(1))
    return sorted_sim

def get_similar_posts_seqno(userid, sim_user):
    sim_posts=[]
    pos = 0
    user_bitmap = get_bitmap(userid)
    a = user_bitmap ^ get_bitmap(sim_user[0][0])
    while True:# SIMILARITY_BITMAP_SIZE:
        pos = a.next_set_bit(pos)
        if pos < 0:
            break
        sim_posts.append(pos)
        pos=pos+1
    logging.info("Similar posts for user %s are %s" %(userid, sim_posts))
    return sim_posts
