# -*- coding: utf-8 -*-
'''
Created on 08.05.2012

@author: dominik
'''
from ConfigParser import ConfigParser
from datetime import datetime
from feedaro.db import db_tools
from feedaro.misc import NewsgrabberSocket, config, PosTagger
from htmlentitydefs import name2codepoint as n2cp # for `decode_htmlentities`
from nltk import regexp_tokenize
from nltk.stem.snowball import GermanStemmer
from time import mktime
import feedparser
import hashlib
import re
import string
from nltk.corpus import BracketParseCorpusReader
import nltk
STOPWORDS_ENG=['a', 'about', 'above', 'above', 'across', 'after', 'afterwards', 'again', 'against', 'all', 'almost', 'alone', 'along', 'already', 'also','although','always','am','among', 'amongst', 'amoungst', 'amount',  'an', 'and', 'another', 'any','anyhow','anyone','anything','anyway', 'anywhere', 'are', 'around', 'as',  'at', 'back','be','became', 'because','become','becomes', 'becoming', 'been', 'before', 'beforehand', 'behind', 'being', 'below', 'beside', 'besides', 'between', 'beyond', 'bill', 'both', 'bottom','but', 'by', 'call', 'can', 'cannot', 'cant', 'co', 'con', 'could', 'couldnt', 'cry', 'de', 'describe', 'detail', 'do', 'done', 'down', 'due', 'during', 'each', 'eg', 'eight', 'either', 'eleven','else', 'elsewhere', 'empty', 'enough', 'etc', 'even', 'ever', 'every', 'everyone', 'everything', 'everywhere', 'except', 'few', 'fifteen', 'fify', 'fill', 'find', 'fire', 'first', 'five', 'for', 'former', 'formerly', 'forty', 'found', 'four', 'from', 'front', 'full', 'further', 'get', 'give', 'go', 'had', 'has', 'hasnt', 'have', 'he', 'hence', 'her', 'here', 'hereafter', 'hereby', 'herein', 'hereupon', 'hers', 'herself', 'him', 'himself', 'his', 'how', 'however', 'hundred', 'ie', 'if', 'in', 'inc', 'indeed', 'interest', 'into', 'is', 'it', 'its', 'itself', 'just', 'keep', 'last', 'latter', 'latterly', 'least', 'less', 'ltd', 'made', 'many', 'may', 'me', 'meanwhile', 'might', 'mill', 'mine', 'more', 'moreover', 'most', 'mostly', 'move', 'much', 'must', 'my', 'myself', 'name', 'namely', 'neither', 'never', 'nevertheless', 'next', 'nine', 'no', 'nobody', 'none', 'noone', 'nor', 'not', 'nothing', 'now', 'nowhere', 'of', 'off', 'often', 'on', 'once', 'one', 'only', 'onto', 'or', 'other', 'others', 'otherwise', 'our', 'ours', 'ourselves', 'out', 'over', 'own','part', 'per', 'perhaps', 'please', 'put', 'rather', 're', 'same', 'see', 'seem', 'seemed', 'seeming', 'seems', 'serious', 'several', 'she', 'should', 'show', 'side', 'since', 'sincere', 'six', 'sixty', 'so', 'some', 'somehow', 'someone', 'something', 'sometime', 'sometimes', 'somewhere', 'still', 'such', 'system', 'take', 'ten', 'than', 'that', 'the', 'their', 'them', 'themselves', 'then', 'thence', 'there', 'thereafter', 'thereby', 'therefore', 'therein', 'thereupon', 'these', 'they', 'thickv', 'thin', 'third', 'this', 'those', 'though', 'three', 'through', 'throughout', 'thru', 'thus', 'to', 'together', 'too', 'top', 'toward', 'towards', 'twelve', 'twenty', 'two', 'un', 'under', 'until', 'up', 'upon', 'us', 'very', 'via', 'was', 'we', 'well', 'were', 'what', 'whatever', 'when', 'whence', 'whenever', 'where', 'whereafter', 'whereas', 'whereby', 'wherein', 'whereupon', 'wherever', 'whether', 'which', 'while', 'whither', 'who', 'whoever', 'whole', 'whom', 'whose', 'why', 'will', 'with', 'within', 'without', 'would', 'yet', 'you', 'your', 'yours', 'yourself', 'yourselves', 'the']
STOPWORDS_GER_UTF8 =['nbsp', 'ouml', 'auml', 'uuml', 'ab', 'aber', '\xc3\xa4hnlich', 'alle', 'allein', 'allem', 'aller', 'alles', 'allg', 'allgemein', 'als', 'also', 'am', 'an', 'and', 'andere', 'anderes', 'auch', 'auf', 'aus', 'au\xc3\x9fer', 'been', 'bei', 'beim', 'besonders', 'bevor', 'bietet', 'bis', 'bzw', 'da', 'dabei', 'dadurch', 'daf\xc3\xbcr', 'daher', 'dann', 'daran', 'darauf', 'daraus', 'das', 'da\xc3\x9f', 'davon', 'davor', 'dazu', 'dem', 'den', 'denen', 'denn', 'dennoch', 'der', 'derem', 'deren', 'des', 'deshalb', 'die', 'dies', 'diese', 'diesem', 'diesen', 'dieser', 'dieses', 'doch', 'dort', 'durch', 'eben', 'ein', 'eine', 'einem', 'einen', 'einer', 'eines', 'einfach', 'er', 'es', 'etc', 'etwa', 'etwas', 'for', 'f\xc3\xbcr', 'ganz', 'ganze', 'ganzem', 'ganzen', 'ganzer', 'ganzes', 'gar', 'gleich', 'gute', 'hat', 'hinter', 'ihm', 'ihr', 'ihre', 'ihrem', 'ihren', 'ihrer', 'ihres', 'im', 'in', 'ist', 'ja', 'je', 'jede', 'jedem', 'jeden', 'jeder', 'jedes', 'jene', 'jenem', 'jenen', 'jener', 'jenes', 'jetzt', 'kann', 'kein', 'keine', 'keinem', 'keinen', 'keiner', 'keines', 'kommen', 'kommt', 'k\xc3\xb6nnen', 'leicht', 'machen', 'man', 'mehr', 'mehrere', 'meist', 'mit', 'mu\xc3\x9f', 'nach', 'neu', 'neue', 'neuem', 'neuen', 'neuer', 'neues', 'nicht', 'noch', 'nur', 'ob', 'oder', 'of', 'ohne', 'per', 'schwierig', 'sehr', 'sein', 'seinem', 'seinen', 'seiner', 'seines', 'seit', 'selbst', 'sich', 'sie', 'sind', 'so', 'soda\xc3\x9f', 'solch', 'solche', 'solchem', 'solchen', 'solcher', 'solches', 'sollte', 'sollten', 'soviel', 'sowohl', 'statt', '\xc3\xbcber', 'um', 'und', 'uns', 'unser', 'unsere', 'unseren', 'unseres', 'unter', 'viel', 'viele', 'vom', 'von', 'vor', 'wann', 'war', 'was', 'wenig', 'wenige', 'weniger', 'wenn', 'wer', 'wie', 'wieder', 'wieviel', 'wird', 'wirklich', 'wo', 'wurde', 'wurden', 'zu', 'zum', 'zur', 'zwischen', 'ab', 'bei', 'da', 'deshalb', 'ein', 'f\xc3\xbcr', 'haben', 'hier', 'ich', 'ja', 'kann', 'machen', 'muesste', 'nach', 'oder', 'seid', 'sonst', 'und', 'vom', 'wann', 'wenn', 'wie', 'zu', 'bin', 'eines', 'hat', 'manche', 'solches', 'an', 'anderm', 'bis', 'das', 'deinem', 'demselben', 'dir', 'doch', 'einig', 'er', 'eurer', 'hatte', 'ihnen', 'ihre', 'ins', 'jenen', 'keinen', 'manchem', 'meinen', 'nichts', 'seine', 'soll', 'unserm', 'welche', 'werden', 'wollte', 'w\xc3\xa4hrend', 'alle', 'allem', 'allen', 'aller', 'alles', 'als', 'also', 'am', 'ander', 'andere', 'anderem', 'anderen', 'anderer', 'anderes', 'andern', 'anderr', 'anders', 'auch', 'auf', 'aus', 'bist', 'bsp.', 'daher', 'damit', 'dann', 'dasselbe', 'dazu', 'da\xc3\x9f', 'dein', 'deine', 'deinen', 'deiner', 'deines', 'dem', 'den', 'denn', 'denselben', 'der', 'derer', 'derselbe', 'derselben', 'des', 'desselben', 'dessen', 'dich', 'die', 'dies', 'diese', 'dieselbe', 'dieselben', 'diesem', 'diesen', 'dieser', 'dieses', 'dort', 'du', 'durch', 'eine', 'einem', 'einen', 'einer', 'einige', 'einigem', 'einigen', 'einiger', 'einiges', 'einmal', 'es', 'etwas', 'euch', 'euer', 'eure', 'eurem', 'euren', 'eures', 'ganz', 'ganze', 'ganzen', 'ganzer', 'ganzes', 'gegen', 'gemacht', 'gesagt', 'gesehen', 'gewesen', 'gewollt', 'hab', 'habe', 'hatten', 'hin', 'hinter', 'ihm', 'ihn', 'ihr', 'ihrem', 'ihren', 'ihrer', 'ihres', 'im', 'in', 'indem', 'ist', 'jede', 'jedem', 'jeden', 'jeder', 'jedes', 'jene', 'jenem', 'jener', 'jenes', 'jetzt', 'kein', 'keine', 'keinem', 'keiner', 'keines', 'konnte', 'k\xc3\xb6nnen', 'k\xc3\xb6nnte', 'mache', 'machst', 'macht', 'machte', 'machten', 'man', 'manchen', 'mancher', 'manches', 'mein', 'meine', 'meinem', 'meiner', 'meines', 'mich', 'mir', 'mit', 'muss', 'musste', 'm\xc3\xbc\xc3\x9ft', 'nicht', 'noch', 'nun', 'nur', 'ob', 'ohne', 'sage', 'sagen', 'sagt', 'sagte', 'sagten', 'sagtest', 'sehe', 'sehen', 'sehr', 'seht', 'sein', 'seinem', 'seinen', 'seiner', 'seines', 'selbst', 'sich', 'sicher', 'sie', 'sind', 'so', 'solche', 'solchem', 'solchen', 'solcher', 'sollte', 'sondern', 'um', 'uns', 'unse', 'unsen', 'unser', 'unses', 'unter', 'viel', 'von', 'vor', 'war', 'waren', 'warst', 'was', 'weg', 'weil', 'weiter', 'welchem', 'welchen', 'welcher', 'welches', 'werde', 'wieder', 'will', 'wir', 'wird', 'wirst', 'wo', 'wolle', 'wollen', 'wollt', 'wollten', 'wolltest', 'wolltet', 'w\xc3\xbcrde', 'w\xc3\xbcrden', 'z.B.', 'zum', 'zur', 'zwar', 'zwischen', '\xc3\xbcber', 'aber', 'abgerufen', 'abgerufene', 'abgerufener', 'abgerufenes', 'acht', 'acute', 'allein', 'allerdings', 'allerlei', 'allg', 'allgemein', 'allm\xc3\xa4hlich', 'allzu', 'alsbald', 'amp', 'and', 'andererseits', 'andernfalls', 'anerkannt', 'anerkannte', 'anerkannter', 'anerkanntes', 'anfangen', 'anfing', 'angefangen', 'angesetze', 'angesetzt', 'angesetzten', 'angesetzter', 'ansetzen', 'anstatt', 'arbeiten', 'aufgeh\xc3\xb6rt', 'aufgrund', 'aufh\xc3\xb6ren', 'aufh\xc3\xb6rte', 'aufzusuchen', 'ausdr\xc3\xbccken', 'ausdr\xc3\xbcckt', 'ausdr\xc3\xbcckte', 'ausgenommen', 'ausser', 'ausserdem', 'author', 'autor', 'au\xc3\x9fen', 'au\xc3\x9fer', 'au\xc3\x9ferdem', 'au\xc3\x9ferhalb', 'background', 'bald', 'bearbeite', 'bearbeiten', 'bearbeitete', 'bearbeiteten', 'bedarf', 'bedurfte', 'bed\xc3\xbcrfen', 'been', 'befragen', 'befragte', 'befragten', 'befragter', 'begann', 'beginnen', 'begonnen', 'behalten', 'behielt', 'beide', 'beiden', 'beiderlei', 'beides', 'beim', 'beinahe', 'beitragen', 'beitrugen', 'bekannt', 'bekannte', 'bekannter', 'bekennen', 'benutzt', 'bereits', 'berichten', 'berichtet', 'berichtete', 'berichteten', 'besonders', 'besser', 'bestehen', 'besteht', 'betr\xc3\xa4chtlich', 'bevor', 'bez\xc3\xbcglich', 'bietet', 'bisher', 'bislang', 'biz', 'bleiben', 'blieb', 'bloss', 'blo\xc3\x9f', 'border', 'brachte', 'brachten', 'brauchen', 'braucht', 'bringen', 'br\xc3\xa4uchte', 'bzw', 'b\xc3\xb6den', 'ca', 'ca.', 'collapsed', 'com', 'comment', 'content', 'da?', 'dabei', 'dadurch', 'daf\xc3\xbcr', 'dagegen', 'dahin', 'damals', 'danach', 'daneben', 'dank', 'danke', 'danken', 'dannen', 'daran', 'darauf', 'daraus', 'darf', 'darfst', 'darin', 'darum', 'darunter', 'dar\xc3\xbcber', 'dar\xc3\xbcberhinaus', 'dass', 'davon', 'davor', 'demnach', 'denen', 'dennoch', 'derart', 'derartig', 'derem', 'deren', 'derjenige', 'derjenigen', 'derzeit', 'desto', 'deswegen', 'diejenige', 'diesseits', 'dinge', 'direkt', 'direkte', 'direkten', 'direkter', 'doc', 'doppelt', 'dorther', 'dorthin', 'drauf', 'drei', 'drei\xc3\x9fig', 'drin', 'dritte', 'drunter', 'dr\xc3\xbcber', 'dunklen', 'durchaus', 'durfte', 'durften', 'd\xc3\xbcrfen', 'd\xc3\xbcrfte', 'eben', 'ebenfalls', 'ebenso', 'ehe', 'eher', 'eigenen', 'eigenes', 'eigentlich', 'einba\xc3\xbcn', 'einerseits', 'einfach', 'einf\xc3\xbchren', 'einf\xc3\xbchrte', 'einf\xc3\xbchrten', 'eingesetzt', 'einigerma\xc3\x9fen', 'eins', 'einseitig', 'einseitige', 'einseitigen', 'einseitiger', 'einst', 'einstmals', 'einzig', 'elf', 'ende', 'entsprechend', 'entweder', 'erg\xc3\xa4nze', 'erg\xc3\xa4nzen', 'erg\xc3\xa4nzte', 'erg\xc3\xa4nzten', 'erhalten', 'erhielt', 'erhielten', 'erh\xc3\xa4lt', 'erneut', 'erst', 'erste', 'ersten', 'erster', 'er\xc3\xb6ffne', 'er\xc3\xb6ffnen', 'er\xc3\xb6ffnet', 'er\xc3\xb6ffnete', 'er\xc3\xb6ffnetes', 'etc', 'etliche', 'etwa', 'fall', 'falls', 'fand', 'fast', 'ferner', 'finden', 'findest', 'findet', 'folgende', 'folgenden', 'folgender', 'folgendes', 'folglich', 'for', 'fordern', 'fordert', 'forderte', 'forderten', 'fortsetzen', 'fortsetzt', 'fortsetzte', 'fortsetzten', 'fragte', 'frau', 'frei', 'freie', 'freier', 'freies', 'fuer', 'f\xc3\xbcnf', 'gab', 'ganzem', 'gar', 'gbr', 'geb', 'geben', 'geblieben', 'gebracht', 'gedurft', 'geehrt', 'geehrte', 'geehrten', 'geehrter', 'gefallen', 'gefiel', 'gef\xc3\xa4lligst', 'gef\xc3\xa4llt', 'gegeben', 'gehabt', 'gehen', 'geht', 'gekommen', 'gekonnt', 'gemocht', 'gem\xc3\xa4ss', 'genommen', 'genug', 'gern', 'gestern', 'gestrige', 'getan', 'geteilt', 'geteilte', 'getragen', 'gewisserma\xc3\x9fen', 'geworden', 'ggf', 'gib', 'gibt', 'gleich', 'gleichwohl', 'gleichzeitig', 'gl\xc3\xbccklicherweise', 'gmbh', 'gratulieren', 'gratuliert', 'gratulierte', 'gute', 'guten', 'g\xc3\xa4ngig', 'g\xc3\xa4ngige', 'g\xc3\xa4ngigen', 'g\xc3\xa4ngiger', 'g\xc3\xa4ngiges', 'g\xc3\xa4nzlich', 'haette', 'halb', 'hallo', 'hast', 'hattest', 'hattet', 'heraus', 'herein', 'heute', 'heutige', 'hiermit', 'hiesige', 'hinein', 'hinten', 'hinterher', 'hoch', 'html', 'http', 'hundert', 'h\xc3\xa4tt', 'h\xc3\xa4tte', 'h\xc3\xa4tten', 'h\xc3\xb6chstens', 'igitt', 'image', 'immer', 'immerhin', 'important', 'indessen', 'info', 'infolge', 'innen', 'innerhalb', 'insofern', 'inzwischen', 'irgend', 'irgendeine', 'irgendwas', 'irgendwen', 'irgendwer', 'irgendwie', 'irgendwo', 'je', 'jed', 'jedenfalls', 'jederlei', 'jedoch', 'jemand', 'jenseits', 'j\xc3\xa4hrig', 'j\xc3\xa4hrige', 'j\xc3\xa4hrigen', 'j\xc3\xa4hriges', 'kam', 'kannst', 'kaum', 'kei', 'nes', 'keinerlei', 'keineswegs', 'klar', 'klare', 'klaren', 'klares', 'klein', 'kleinen', 'kleiner', 'kleines', 'koennen', 'koennt', 'koennte', 'koennten', 'komme', 'kommen', 'kommt', 'konkret', 'konkrete', 'konkreten', 'konkreter', 'konkretes', 'konnten', 'k\xc3\xb6nn', 'k\xc3\xb6nnt', 'k\xc3\xb6nnten', 'k\xc3\xbcnftig', 'lag', 'lagen', 'langsam', 'lassen', 'laut', 'lediglich', 'leer', 'legen', 'legte', 'legten', 'leicht', 'leider', 'lesen', 'letze', 'letzten', 'letztendlich', 'letztens', 'letztes', 'letztlich', 'lichten', 'liegt', 'liest', 'links', 'l\xc3\xa4ngst', 'l\xc3\xa4ngstens', 'mag', 'magst', 'mal', 'mancherorts', 'manchmal', 'mann', 'margin', 'med', 'mehr', 'mehrere', 'meist', 'meiste', 'meisten', 'meta', 'mindestens', 'mithin', 'mochte', 'morgen', 'morgige', 'muessen', 'muesst', 'musst', 'mussten', 'mu\xc3\x9f', 'mu\xc3\x9ft', 'm\xc3\xb6chte', 'm\xc3\xb6chten', 'm\xc3\xb6chtest', 'm\xc3\xb6gen', 'm\xc3\xb6glich', 'm\xc3\xb6gliche', 'm\xc3\xb6glichen', 'm\xc3\xb6glicher', 'm\xc3\xb6glicherweise', 'm\xc3\xbcssen', 'm\xc3\xbcsste', 'm\xc3\xbcssten', 'm\xc3\xbc\xc3\x9fte', 'nachdem', 'nacher', 'nachhinein', 'nahm', 'nat\xc3\xbcrlich', 'ncht', 'neben', 'nebenan', 'nehmen', 'nein', 'neu', 'neue', 'neuem', 'neuen', 'neuer', 'neues', 'neun', 'nie', 'niemals', 'niemand', 'nimm', 'nimmer', 'nimmt', 'nirgends', 'nirgendwo', 'nter', 'nutzen', 'nutzt', 'nutzung', 'n\xc3\xa4chste', 'n\xc3\xa4mlich', 'n\xc3\xb6tigenfalls', 'n\xc3\xbctzt', 'oben', 'oberhalb', 'obgleich', 'obschon', 'obwohl', 'oft', 'online', 'org', 'padding', 'per', 'pfui', 'pl\xc3\xb6tzlich', 'pro', 'reagiere', 'reagieren', 'reagiert', 'reagierte', 'rechts', 'regelm\xc3\xa4\xc3\x9fig', 'rief', 'rund', 'sang', 'sangen', 'schlechter', 'schlie\xc3\x9flich', 'schnell', 'schon', 'schreibe', 'schreiben', 'schreibens', 'schreiber', 'schwierig', 'sch\xc3\xa4tzen', 'sch\xc3\xa4tzt', 'sch\xc3\xa4tzte', 'sch\xc3\xa4tzten', 'sechs', 'sect', 'sehrwohl', 'sei', 'seit', 'seitdem', 'seite', 'seiten', 'seither', 'selber', 'senke', 'senken', 'senkt', 'senkte', 'senkten', 'setzen', 'setzt', 'setzte', 'setzten', 'sicherlich', 'sieben', 'siebte', 'siehe', 'sieht', 'singen', 'singt', 'sobald', 'soda\xc3\x9f', 'soeben', 'sofern', 'sofort', 'sog', 'sogar', 'solange', 'solc', 'hen', 'solch', 'sollen', 'sollst', 'sollt', 'sollten', 'solltest', 'somit', 'sonstwo', 'sooft', 'soviel', 'soweit', 'sowie', 'sowohl', 'spielen', 'sp\xc3\xa4ter', 'startet', 'startete', 'starteten', 'statt', 'stattdessen', 'steht', 'steige', 'steigen', 'steigt', 'stets', 'stieg', 'stiegen', 'such', 'suchen', 's\xc3\xa4mtliche', 'tages', 'tat', 'tats\xc3\xa4chlich', 'tats\xc3\xa4chlichen', 'tats\xc3\xa4chlicher', 'tats\xc3\xa4chliches', 'tausend', 'teile', 'teilen', 'teilte', 'teilten', 'titel', 'total', 'trage', 'tragen', 'trotzdem', 'trug', 'tr\xc3\xa4gt', 'tun', 'tust', 'tut', 'txt', 't\xc3\xa4t', 'ueber', 'umso', 'unbedingt', 'ungef\xc3\xa4hr', 'unm\xc3\xb6glich', 'unm\xc3\xb6gliche', 'unm\xc3\xb6glichen', 'unm\xc3\xb6glicher', 'unn\xc3\xb6tig', 'unsem', 'unser', 'unsere', 'unserem', 'unseren', 'unserer', 'unseres', 'unten', 'unterbrach', 'unterbrechen', 'unterhalb', 'unwichtig', 'usw', 'var', 'vergangen', 'vergangene', 'vergangener', 'vergangenes', 'vermag', 'vermutlich', 'verm\xc3\xb6gen', 'verrate', 'verraten', 'verriet', 'verrieten', 'version', 'versorge', 'versorgen', 'versorgt', 'versorgte', 'versorgten', 'versorgtes', 'ver\xc3\xb6ffentlichen', 'ver\xc3\xb6ffentlicher', 'ver\xc3\xb6ffentlicht', 'ver\xc3\xb6ffentlichte', 'ver\xc3\xb6ffentlichten', 'ver\xc3\xb6ffentlichtes', 'viele', 'vielen', 'vieler', 'vieles', 'vielleicht', 'vielmals', 'vier', 'vollst\xc3\xa4ndig', 'voran', 'vorbei', 'vorgestern', 'vorher', 'vorne', 'vor\xc3\xbcber', 'v\xc3\xb6llig', 'w\xc3\xa4hrend', 'wachen', 'waere', 'warum', 'weder', 'wegen', 'weitere', 'weiterem', 'weiteren', 'weiterer', 'weiteres', 'weiterhin', 'wei\xc3\x9f', 'wem', 'wen', 'wenig', 'wenige', 'weniger', 'wenigstens', 'wenngleich', 'wer', 'werdet', 'weshalb', 'wessen', 'wichtig', 'wieso', 'wieviel', 'wiewohl', 'willst', 'wirklich', 'wodurch', 'wogegen', 'woher', 'wohin', 'wohingegen', 'wohl', 'wohlweislich', 'womit', 'woraufhin', 'woraus', 'worin', 'wurde', 'wurden', 'w\xc3\xa4hrenddessen', 'w\xc3\xa4r', 'w\xc3\xa4re', 'w\xc3\xa4ren', 'zahlreich', 'zehn', 'zeitweise', 'ziehen', 'zieht', 'zog', 'zogen', 'zudem', 'zuerst', 'zufolge', 'zugleich', 'zuletzt', 'zumal', 'zur\xc3\xbcck', 'zusammen', 'zuviel', 'zwanzig', 'zwei', 'zw\xc3\xb6lf', '\xc3\xa4hnlich', '\xc3\xbcbel', '\xc3\xbcberall', '\xc3\xbcberallhin', '\xc3\xbcberdies', '\xc3\xbcbermorgen', '\xc3\xbcbrig', '\xc3\xbcbrigens']
STOPWORDS_GER = [w.decode('utf-8') for w in STOPWORDS_GER_UTF8]
RE_HTML_ENTITY = re.compile(r'&(#?)(x?)(\w+);', re.UNICODE)

pos_tagger=None
def decode_htmlentities(text):
    """
    Decode HTML entities in text, coded as hex, decimal or named.
    
    Adapted from http://github.com/sku/python-twitter-ircbot/blob/321d94e0e40d0acc92f5bf57d126b57369da70de/html_decode.py
    
    >>> u = u'E tu vivrai nel terrore - L&#x27;aldil&#xE0; (1981)'
    >>> print decode_htmlentities(u).encode('UTF-8')
    E tu vivrai nel terrore - L'aldilà (1981)
    >>> print decode_htmlentities("l&#39;eau")
    l'eau
    >>> print decode_htmlentities("foo &lt; bar")
    foo < bar
    
    """
    def substitute_entity(match):
        ent = match.group(3)
        if match.group(1) == "#":
            # decoding by number
            if match.group(2) == '':
                # number is in decimal
                return unichr(int(ent))
            elif match.group(2) == 'x':
                # number is in hex
                return unichr(int('0x' + ent, 16))
        else:
            # they were using a name
            cp = n2cp.get(ent)
            if cp:
                return unichr(cp)
            else:
                return match.group()

    try:
        return RE_HTML_ENTITY.sub(substitute_entity, text)
    except:
        # in case of errors, return input
        # e.g., ValueError: unichr() arg not in range(0x10000) (narrow Python build)
        return text

def filter_text(raw):
    """
    Filter out wiki mark-up from `raw`, leaving only text. `raw` is either unicode
    or utf-8 encoded string.
    """
    # parsing of the wiki markup is not perfect, but sufficient for our purposes
    # contributions to improving this code are welcome :)
    text = decode_htmlentities(any2unicode(raw, 'utf8', errors='ignore'))
    text = decode_htmlentities(text) # '&amp;nbsp;' --> '\xa0'
    return text
    
def any2unicode(text, encoding='utf8', errors='strict'):
    """Convert a string (bytestring in `encoding` or unicode), to unicode."""
    if isinstance(text, unicode):
        return text
    return unicode(text, encoding, errors=errors)

def tag_pos_post(words):
    global pos_tagger
    if pos_tagger==None:
        pos_tagger=PosTagger.PosTagger()
    return pos_tagger.tag(words)

def sanitize_posts(text):
    text = filter_text(text)
    ''' Takes some text and sanitizes it
        returns list of words
    '''
    pattern =r'''
        (?x)                    # set verbose flag to allow verbose regexp
        ([A-Z]\.)+                # abbreviations, e.g. U.S.A.
        | \w+(-\w+)*            # words with optional internal hyphens
        | \$?\d+(\.\d+)?%?        # currency and percentages, e.g. $12.40, 82%
        #| \.\.\.                # elipsis
        #| [][.,;"'?():-_]        # these are seperate tokens
    '''
    text = re.sub('<[^>]*>', '', text)
    words = regexp_tokenize(text, pattern)    
#    lower_words = [w.lower() for w in words if w[0] in string.uppercase]
   # stemmer = GermanStemmer("german")
  #  lower_words = [stemmer.stem(w.lower()) for w in words]
    #lemmer=[Baseform(w) for w in words]
    #lower_words =[b[0][0].lower() for b in lemmer if b!=[] and (b[0][1]=='N' or b[0][1]=='V')]
    lower_words=tag_pos_post(words)
    sanitized_posts = [w for w in lower_words
                        if w not in STOPWORDS_GER_UTF8 
                        if w not in STOPWORDS_ENG
                        if len(w) > 2 ]
    print sanitized_posts
    return sanitized_posts

class Post(dict):
    def __hash__(self):
        return self['link'].__hash__()
    def hash(self):
        return hashlib.sha1(self['link']).hexdigest()
    
class Feed(object):
    def __init__(self,url,title,description,version,link,lang,date=datetime.now(),id=None,post_hashes=[],name=None):
        self._url=url
        self._title=title
        self._description=description
        self._version=version
        self._date=date
        self._link=link
        self._lang=lang
        self._id=id
        self._post_hashes=post_hashes
        self._name=name
    
    def __str__(self):
        return "Feed: %s: %s" %(self._url,self._post_hashes)
    
    def get_posts(self):
        parsed = feedparser.parse(self._url)
        foo = self._normalize_posts(parsed)
        _set = set(foo)
        return _set
    
    def get_new_posts(self):
        return [post for post in self.get_posts() if post['hash'] not in self._post_hashes]
    
    def _normalize_posts(self,parsed):
        norm_posts=[]
        for p in parsed.entries:
            parsed_date = p.get('published_parsed',None)
            if parsed_date == None:
                parsed_date = datetime.now()
            else:
                parsed_date = datetime.fromtimestamp(mktime(parsed_date))
            full_text=""
            san_full=""
            try:
                full_text = NewsgrabberSocket.getText(p.link)
                san_full = sanitize_posts(full_text)
            except Exception as e:
                print "Socket error: %s for %s" % (e,p['title'])      
            if 'description' not in p:
                #catch the case when desscription is in title_detail.value like in the feed of BRIGITTE
                if 'title_detail' in p:
                    if 'value' in p['title_detail']:
                        p.description = p['title_detail']['value']
                else:
                    p.description = ''
            if 'title' not in p:
                p.title = 'no title'
            if 'link' not in p:
                p.title = 'no link'
                
            post =Post({
                   'title':p.title,
                   'description':p.description,
                   'san_desc':sanitize_posts(p.description),
                   'link':p.link,
                   'date':parsed_date,
                   'feed_url':self._url,
                   'feed_name':self._name,
                   'feed_id':self._id,
                   'full_text':full_text,
                   'san_full':san_full,
                   "seqno":db_tools.counter("posts")
                   })
            post['hash']=post.hash()
            norm_posts.append(post)
        return norm_posts
    
    @staticmethod
    def getInstance(feed_url,name=None):
            parsed_feed = feedparser.parse(feed_url)
            title = parsed_feed.feed.get('title', '')
            link = parsed_feed.feed.get('link', '')
            description = parsed_feed.feed.get('description', '')
            version = parsed_feed.feed.get('version', '')
            lang = parsed_feed.feed.get('language','')
            return Feed(feed_url,title,description,version,link,lang,name=name)
    @staticmethod
    def getFromDict(normal_dict):
        dict = Post(normal_dict)
        return Feed(dict['url'],dict['title'],dict['desc'],dict['version'], dict['link'] ,dict['lang'], dict['date'],id=dict['_id'],post_hashes=dict['post_hashes'],name=dict['name'])
                                                                          
