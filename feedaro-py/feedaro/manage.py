'''
Created on 09.05.2012

@author: dominik
'''
import argparse
from feed.feed_tools import Feed
from argparse import RawTextHelpFormatter
import misc.ConsoleOutput #@UnusedImport
from feedaro.lda import lda_tools
from feedaro.db import db_tools
from feedaro.misc import config
import logging
from logging.config import dictConfig
from feedaro.lda.lda_tools import Dicitionary,Model, PostModel, FeedCorpus, FeedModel
from feedaro.similarity.sim import Similarity
from feedaro.stats import stats_tools


def update_db_sims():
    dict=Dicitionary(create_new=False)
    model=PostModel(dict, load=True)
    sim = Similarity(dict, model, model.corpus)
    sim.write_similarity_list(10)
def print_sims():
    dict=Dicitionary(create_new=False)
    model=PostModel(dict, load=True)
    sim = Similarity(dict, model, model.corpus)
    all_sim = sim.get_similarity_list(10)
    for s in all_sim:
        print s
def eval_sim():
    print "test1"
    dict=Dicitionary(create_new=False)
    print "test2"
    model=PostModel(dict, load=True)
    print "test3"
    sim = Similarity(dict, model, model.corpus)
    print "test4"
    #sim.write_similarity_list()
    sim.eval_post_sim()
    
def _addFeedFromFile(file_name):
    f = open(file_name)
    for line in f:
        (name, url)=line.split()
        f = Feed.getInstance(url, name)
        db_tools.add_feed(f)
    

def addFeed(url,name, file):
    if file == None:
        f = Feed.getInstance(url,name)
        db_tools.add_feed(f)
    else:
        _addFeedFromFile(file)
    
def delFeed(url,isUrl):
    if isUrl:
        db_tools.get_feeds().remove({"url":url})
    else:
        db_tools.get_feeds().remove({"name":url})
        
def updateFeed(url,isUrl,verbosity):
    feeds = None
    if(url == 'all'):
        feeds = db_tools.get_feeds().find()
    else:
        if isUrl:
            feeds =db_tools.get_feeds().find({"url":url})
        else:
            feeds =db_tools.get_feeds().find({"name":url})
    
    for feed in feeds:
        f = Feed.getFromDict(feed)
        posts = f.get_new_posts()
        _printPosts(posts, verbosity)
        db_tools.add_posts(posts, f)
        
def listFeed(url,isUrl,verbosity):
    feeds=None
    if(url == 'all'):
        feeds= db_tools.get_feeds().find()
    else:
        if isUrl:
            feeds =db_tools.get_feeds().find({"url":url})
        else:
            feeds =db_tools.get_feeds().find({"name":url})
    for feed in feeds:
        print "title: %s" % feed['title']
        if verbosity > 0:
            print "name:          %s" % feed['name']
            print "url:          %s" % feed['url']
            print "lang:         %s" %feed['lang']
            print "dist:         %s" %feed['dist']
        if verbosity > 1:
            print "link:         %s" %feed['link']
            print "date:         %s" %feed['date']
            print "desc:         %s"%feed['desc']
            print "version:      %s"%feed['version']
        if verbosity > 2:
            print "post_hashes:  %s" % feed['post_hashes']
        print "--------------------------------"
            

def listPosts(url,isUrl,verbosity):
    posts = None
    if url=='all':
        posts = db_tools.get_posts().find()
    else:
        if isUrl:
            posts=db_tools.get_posts().find({"url":url})
        else:
            posts=db_tools.get_posts().find({"feed_name":url})
    _printPosts(posts,verbosity)

            
def _printPosts(posts,verbosity):            
    for post in posts:
        print "title: %s @ %s" % (post['title'], post['feed_name'])
        if verbosity >0:
            print "link:         %s" % post['link']
            print "date:         %s" % post['date']
            print "feed_url:     %s" % post['feed_url']
            #print "id:           %s" % post['_id']
        if verbosity > 1:
            try:
                print "dist:         %s" %sorted(post['dist'],key=lambda dist: -dist[1])
            except KeyError:
                pass
            print "feed_id:      %s" % post['feed_id']
            print "san_full:     %s" % post['san_full']
        if verbosity > 2:
            print "san_desc:     %s" % post['san_desc']
            print "desc:         %s" % post['description']
        print "--------------------------------"
        
def count_posts():
    print db_tools.get_posts_count()
    
def add_seq_no():
    logging.info("Adding sequence numbers to all posts without one")
    posts = db_tools.get_posts().find({'seqno':{'$exists':False}})
    for post in posts:
        db_tools.addseqno2post(post["_id"])
    
def init_lda(topic):
    print topic
    dict = lda_tools.Dicitionary(create_new=True)
    lda_model=lda_tools.PostModel(dict)
    lda_model.learn_lda_model(topic_num=topic)
def init_feeds(topic):
    print topic
    dict = lda_tools.Dicitionary(create_new=True)
    lda_model=lda_tools.FeedModel(dict,name="feedaro_feeds",corpus=FeedCorpus(dict))
    lda_model.learn_lda_model(topic_num=5)        
def learn_lda(topic):
    dict = lda_tools.Dicitionary(create_new=False)
    lda_model=lda_tools.PostModel(dict,load=True)
    lda_model.learn_lda_model(topic_num=topic)
    
def get_lda_topics(id=-1):
    dict = lda_tools.Dicitionary(create_new=False)
    lda_model=lda_tools.PostModel(dict,load=True)
    if id < 0:
        topics = lda_model.get_topics()
        for i,topic in enumerate(topics):
            logging.info("%s:%s" %(i,topic) )
    else:
        topic = lda_model.get_topics()[id]
        print topic

def get_lda_topic_count():
    dict = lda_tools.Dicitionary(create_new=False)
    lda_model=lda_tools.PostModel(dict,load=True)
    topics = lda_model.get_topic_count()
    topic_names=lda_model.get_topics()
    for i,topic in enumerate(topics):
        print "%s:%s" %(topic,topic_names[topic[0]])

def get_post_by_topic(topic,threshold):
    dict = lda_tools.Dicitionary(create_new=False)
    lda_model=lda_tools.PostModel(dict,load=True)
    posts = lda_model.get_post_by_topic(topic,threshold)
    print "Results for topic", topic
    _printPosts(posts,0)
        
def eval_posts():
    dict = lda_tools.Dicitionary(create_new=False)
    lda_model=lda_tools.PostModel(dict,load=True)
    lda_model.evaluate_posts()
    
def eval_feeds():
    dict = lda_tools.Dicitionary(create_new=False)
    lda_model=lda_tools.FeedModel(dict, name="feedaro_feeds", load=True, corpus=FeedCorpus(dict))
    lda_model.evaluate_posts()
    
def update_dic():
    dict = lda_tools.Dicitionary(create_new=False)
    dict.update_dictionary()
    
def initialize_redis():
    stats_tools.init_redis()
    
def __main__():
    dictConfig(config.LOG_CONF)
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help =" sub-commands")
    
    parser_feed = subparsers.add_parser("feed", help="manage Feeds",formatter_class=RawTextHelpFormatter)
    parser_feed.add_argument("feed_execute", help="add: adds a new feed <feed> <feed_name> \ndel: deletes a feed <feed_name> \nupdate: download new posts \nlist: prints all saved feeds\n\n", choices=["add","del","update","list"])
    parser_feed.add_argument("feed", help="feed url or name or file", nargs="?", default="all")
    parser_feed.add_argument("feed_name", help="feed name", nargs="?", default=None)
    parser_feed.add_argument("-v","--verbosity", help="Level of verbosity",  type=int, choices=[0,1,2,3], default=0)
    parser_feed.add_argument("-u","--url",help="Search by URL instead of namee", action="store_true")
    parser_feed.add_argument("-f","--file",help="Load a file with rss urls", default=None)

    
    parser_post = subparsers.add_parser("post", help="manage Posts")
    parser_post.add_argument("post_execute", help="command to execute", choices=["list","count","seqno"])
    parser_post.add_argument("feed", help="feed name", nargs="?" ,default="all")
    parser_post.add_argument("-v","--verbosity", type=int, choices=[0,1,2,3], default=0)
    parser_post.add_argument("-u","--url",help="use if u want to give a url instead a name", action="store_true")
        
    parser_lda = subparsers.add_parser("lda", help="control LDA algorithm")
    parser_lda.add_argument("lda_execute", help="command to execute", choices=["init","init-feeds", "learn", "topics","topic-count", "eval", "up_dic", "by-topic","eval-feeds"])
    parser_lda.add_argument("topic_id",type=int,default=-1,nargs="?")
    parser_lda.add_argument("threshold",type=float,default=0.5,nargs="?")
    parser_lda.add_argument("-t","--topic", type=int, choices=[2,10,20,50,100,200,500], default=200)
    
    
    parser_sim = subparsers.add_parser("redis", help="initialize statistics")
    parser_sim.add_argument("redis_execute", help="command to execute", choices=["init"])
    
    parser_sim = subparsers.add_parser("sim", help="control SIM algorithm")
    parser_sim.add_argument("sim_execute", help="command to execute", choices=["sims", "eval"])
    
    
    args = parser.parse_args()
    
    if("feed_execute" in args):
        if(args.feed_execute == 'add'):
            addFeed(args.feed,args.feed_name,args.file)
        if(args.feed_execute == 'del'):
            delFeed(args.feed,args.url)
        if(args.feed_execute == 'update'):
            updateFeed(args.feed,args.url,args.verbosity)
        if(args.feed_execute == 'list'):
            listFeed(args.feed,args.url,args.verbosity)
        
    else:
        if("post_execute" in args):
            if(args.post_execute=='list'):
                listPosts(args.feed,args.url,args.verbosity)
            if(args.post_execute=='count'):
                count_posts()
            if(args.post_execute=='seqno'):
                add_seq_no()
                
        else:
            if("lda_execute" in args):
                if(args.lda_execute == 'init'):
                    init_lda(args.topic)
                if(args.lda_execute == 'learn'):
                    learn_lda(args.topic)
                if(args.lda_execute == 'topics'):
                    get_lda_topics(args.topic_id)
                if(args.lda_execute == 'topic-count'):
                    get_lda_topic_count()
                if(args.lda_execute == 'by-topic'):
                    get_post_by_topic(args.topic_id, args.threshold)
                if(args.lda_execute == 'eval'):
                    eval_posts()
                if(args.lda_execute == 'up_dic'):
                    update_dic()
                if(args.lda_execute=='eval-feeds'):
                    eval_feeds()
                if(args.lda_execute=='init-feeds'):
                    init_feeds(args.topic)
            else:
                if("sim_execute" in args):
                    if args.sim_execute =='sims':
                        print_sims()
                    if args.sim_execute =='eval':
                        eval_sim()
                else:
                    if("redis_execute" in args):
                        if args.redis_execute =='init':
                            initialize_redis()

if __name__ == '__main__':
    __main__()
