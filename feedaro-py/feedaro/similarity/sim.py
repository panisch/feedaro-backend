'''
Created on 26.05.2012

@author: dominik
'''
from gensim import similarities
from feedaro.db import db_tools
class Similarity:
    def __init__(self,dictionary,model,corpus):
        self._dicitionary=dictionary.lda_dict()
        self._model=model.lda_model()
        self._sim_index=similarities.SparseMatrixSimilarity(self._model[corpus])
        
    def get_similarity(self,document):
        #TODO: Limit to 10 similar posts
        bow = self._dicitionary.doc2bow(document)
        doc = self._model[bow]
        sims = self._sim_index[doc]
        return sims
    

    def get_similarity_list(self,top=10):
        sims=[]
        self._sim_index.num_best=top
        for sim in self._sim_index:
            sims.append(sim)
        return sims
    
    def write_similarity_list(self,top=10):
        sims = self.get_similarity_list(top)
        for i,sim in enumerate(sims):
            db_tools.addSim2Post(i,sim)
            
    def eval_post_sim(self):
        print "starting eval similarity"
        db = db_tools.get_db()
        dist=[]
        sim_posts=[]
        dict = {}
        i=0
        posts = db_tools.get_posts().find()
        for i,post in enumerate(posts):
            dict[i] = post["_id"]
        posts.rewind() 
        for post in posts:
            if len(post['san_full']) != 0:
                dist = post['san_full']
            else:
                if len(post['san_desc']) != 0:
                    dist = post['san_desc']
            sim = self.get_similarity(dist)
            for s in sim:
                sim_posts.append(dict.get(s[0]))
            db_tools.addSim2Post(post['_id'], sim_posts)
            print "processing post: ", i
            i=i+1
            