'''
Created on 08.05.2012

@author: dominik
'''
from pymongo import Connection
from ConfigParser import SafeConfigParser
import os
from feedaro.misc import config
connection = None
def get_db():
    global connection
    if ( connection == None):
        connection = Connection(config.get("mongodb","url"))
    db = connection.feedaro3
    return db
    

def add_feed(feed):
    db = get_db()
    db_feeds = db.feeds
    feed_dict = feed2dict(feed)
    id = db_feeds.insert(feed_dict)
    feed._id=id
    
def update_feed(feed):
    db = get_db()
    db.feeds.update({'_id':feed._id},feed2dict(feed))
    

def get_feeds():
    db = get_db()
    db_feeds = db.feeds
    return db_feeds

def delete_feeds():
    db = get_db()
    db.feeds.drop()
    
def delete_posts():
    db = get_db()
    db.posts.drop()

def add_posts(posts,feed):
    for post in posts:
        add_post(post,feed)
        
def add_post(post,feed):        
    db = get_db()
    id = db.posts.insert(post)
    post["_id"]=id
    db.feeds.update({'_id':post['feed_id']},{'$push': {'post_hashes':post['hash']}} )
    feed._post_hashes.append(post['hash'])

def get_posts():
    return get_db().posts

def get_posts_count():
    return get_db().posts.count()

def get_posts_by_seqno(post_ids):
    sim_posts = []
    for id in post_ids:
        post = get_posts().find_one({"seqno":id})
        sim_posts.append(post)
    return sim_posts

def get_top_topics():
    return get_db().toptopics

def feed2dict(feed):
    feed_dict = {'url':feed._url,
             'title':feed._title,
             'desc':feed._description,
             'version':feed._version,
             'date':feed._date,
             'link':feed._link,
             'lang':feed._lang,
             'name':feed._name,
             'post_hashes':[]
             }
    return feed_dict

def addDist2Post(_id, dist):
    '''saves the topic distribution in the post'''
    db = get_db()
    db.posts.update({"_id" :_id}, {'$set': {"dist":dist}})
    
def addDist2Feed(_id,dist):
    db = get_db()
    db.feeds.update({"_id" :_id}, {'$set': {"dist":dist}})
    
def modelflag2Post(_id, flag):
    db = get_db()
    db.posts.update({"_id" :_id}, {'$set': {"modeled":flag}})
    
def addSim2Post(_id,sim):
    db =get_db()
    db.posts.update({"_id" :_id}, {'$set':{"sim":sim}})

def addvote2post(post_seqno, views):
    print "addvote2post: ", post_seqno, views
    db=get_db()
    db.posts.update({'seqno':post_seqno},{'$set':{'views':views}})
    
def addseqno2post(_id):
    db =get_db()
    db.posts.update({"_id" :_id}, {'$set':{"seqno":counter("posts")}})
    
def counter(name):
    db = get_db()
    ret = db.counters.find_and_modify(query={"_id":name},update={"$inc":{"next":1}},new=True,upsert=True)
    return ret['next']