'''
Created on Jun 24, 2012

@author: dominik
'''
from feedaro.lda import lda_tools
from feedaro.db import db_tools
import datetime



def get_top_topic(weeks=1,topic_num=200,topic_ret=5):
    dict = lda_tools.Dicitionary(create_new=False)
    mondays = get_mondays()
    all_topics=[]
    for i,m in enumerate(mondays):
        if i == len(mondays)-1:
            break
        corpus = lda_tools.PostCorpus(dict, {"date":{"$gte":m,"$lte":mondays[i+1]}})
        model_name="topic_%s_%s"% (weeks,i)
        print model_name
        model = lda_tools.PostModel(dict,load=False,name=model_name,corpus=corpus)
        model.learn_lda_model(topic_num,passes=10)
        topics = model.get_topic_count(selector={"date":{"$gte":m,"$lte":mondays[i+1]}})
        print topics
        topic_names=model.get_topics()
        _topics=[]
        print _topics
        for x,topic in enumerate(topics):
            print topic
            _topics.append({"topic_id":topic,"topic_names":topic_names[topic[0]],"start_monday":m,"end_monday":mondays[i+1]})
        print _topics
        print "####"
        all_topics.append(_topics)
    return all_topics


def get_mondays():
    oldest_post=db_tools.get_posts().find_one({"$query":{},"$orderby":{'date':1}})
    d= oldest_post['date']
    d_start = d -datetime.timedelta(d.weekday(),hours=d.hour, minutes=d.minute, seconds=d.second) 
    now = datetime.datetime.today()   
    mondays = [d_start]
    last_monday=d_start
    while True:
        last_monday=last_monday+datetime.timedelta(7)
        mondays.append(last_monday)
        if now < last_monday:
            break
    return mondays
     
 
def save_top_topics():
    topics = get_top_topic()
    for to in topics:
        db_tools.get_top_topics().insert(to[0])
        db_tools.get_top_topics().insert(to[1])
        db_tools.get_top_topics().insert(to[2])
        db_tools.get_top_topics().insert(to[3])
        db_tools.get_top_topics().insert(to[4])

           
    
save_top_topics()