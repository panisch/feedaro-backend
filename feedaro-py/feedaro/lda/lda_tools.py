'''
Created on May 8, 2012

@author: patrick

todo:
    db.feeds.find({},{posts:true})
    posts[topics]=
    db_tools.update_feed(f)
    
    save doc2bow format instead of san_desc
'''

from gensim import corpora, models
from feedaro.db import db_tools
import logging
import sys
from feedaro.misc import config
import os

class Dicitionary(object):
    def __init__(self,dict_name="feedaro" ,create_new=False, data_selector={}):
        self.name=dict_name
        self.__path=config.get("lda","path")+dict_name+'.dict'
        if not create_new:
            try:
                self._dictionary = corpora.Dictionary.load(self.__path)
            except Exception:
                logging.warn("PATH: %s"%os.path.abspath("."))
                logging.warn("cannot open dict: %s"%self.__path)    
        else:
            self._dictionary = self._create_dictionary(data_selector)
     
    def lda_dict(self):
        return self._dictionary      
      
    def _create_dictionary(self, selector={}):    
        docs=[]
        posts = db_tools.get_posts().find(selector)
        for post in posts:
            db_tools.modelflag2Post(post['_id'], True)
            if 'san_full' not in post:
                logging.debug('Post has no [san_full]: ', post._id )
                continue
                
            san_desc = post['san_full']
            if san_desc != []:
                docs.append(post['san_full'])
        dictionary = corpora.Dictionary(docs)
        try:
            #TODO: Save it in project path
            path= config.get("lda","path")+self.name+'.dict'
            dictionary.save(path)
            logging.debug('LDA dictionary saved to disk: '+path )
        except Exception as e:
            logging.error("LDA dictionary could not be saved: %s" %e)
        return dictionary
    
    def update_dictionary(self):
        posts = db_tools.get_posts().find({'modeled':{'$exists':False}})
        new_docs=[]
        for post in posts:
            db_tools.modelflag2Post(post['_id'], True)
            if 'san_full' not in post:
                logging.debug('Post has no [san_desc]')
                continue
            san_desc = post['san_full']
            if san_desc != []:
                new_docs.append(post['san_full'])
        corpora.Dictionary.add_documents(self._dictionary, new_docs)
        try:
            self._dictionary.save(self.__path)
            logging.debug('new LDA dictionary saved to disk:' +self.__path )
        except Exception as e:
            logging.error("new LDA dictionary could not be saved: %s" %e)
    
class Model(object):
    def __init__(self,dictionary,name="feedaro",load=False,corpus=None):
        self.dictionary=dictionary
        self.name=name
        if corpus ==None:
            corpus=PostCorpus(dictionary)
        self.tfidf = models.TfidfModel(corpus)
        self.corpus= self.tfidf[corpus]
    #    self.corpus=corpus
        if load:
            self._model = models.LdaModel.load(config.get("lda","path")+name+'.model')
    
    def learn_lda_model(self, topic_num = 10, passes=5):
        '''generates and returns lda model'''
        logging.debug('Initializing lda model' )
        self._model = models.LdaModel(self.corpus, id2word=self.dictionary.lda_dict(), 
                                num_topics=topic_num, update_every=0, passes=passes)
        try:
            #TODO: Save it in project path
            path = config.get("lda","path")+self.name+'.model'
            self._model.save(path)
            logging.debug('LDA model saved to disk: ' + path )
        except Exception as e:
            logging.error("LDA model could not be saved: %s" %e)

    def get_topic_distribution(self, clean_document):
        '''takes clean_string, returns topic distributian of clean_string'''
        assert isinstance(clean_document, list), "argument to calc_topic_distributian from Feedcorpus must be of type list %s" % clean_document
        bow_string = self.dictionary.lda_dict().doc2bow(clean_document)
        tfidf_string = self.tfidf[bow_string]
        distro= self._model[tfidf_string]
        return distro
    
    def get_topics(self):
        topics = self._model.show_topics(topics=-1)
        return topics
    

            
    def lda_model(self):
        return self._model

class FeedModel(Model):
    def evaluate_posts(self):
        logging.info('running evaluation of all feeds in db' )
        #FIXME eval all posts for debug
        feeds = db_tools.get_feeds().find()
        for feed in feeds:
            posts = db_tools.get_posts().find({'feed_id':feed["_id"]})
            text = []
            for post in posts:
                text.extend(post['san_full'])
            distro = self.get_topic_distribution(text)
            db_tools.addDist2Feed(feed['_id'], distro)    
                
class PostModel(Model):
    
    def get_topic_count(self,selector={'dist':{"$exists":True}}):
        print "debug"
        print self._model.num_topics
        topic_counts = [[topic,0] for topic in range(self._model.num_topics)]
        posts = db_tools.get_posts().find(selector)
        for post in posts:
            if not 'dist' in post:
                continue
            dists= post['dist']
            for dist in dists:
                #if dist[0]==106:
                 #   print post['title']
                topic_counts[dist[0]][1]+=dist[1]
        topic_counts = sorted(topic_counts, key=lambda item: -item[1])
        return topic_counts 
    
    def get_post_by_topic(self,topic,threshold, selector={'dist':{"$exists":True}}):
        posts = db_tools.get_posts().find(selector)
        ret_list=[]
        for post in posts:
            dists=post['dist']
            for dist in dists:
                if dist[0]==topic:
                    if dist[1] > threshold:
                        ret_list.append(post)
        return ret_list
        
    def evaluate_posts(self):
        logging.info('running evaluation of all posts in db' )
        #FIXME eval all posts for debug
        posts = db_tools.get_posts().find()
        for post in posts:
            if len(post['san_full']) != 0:
                distro = self.get_topic_distribution(post['san_full'])
            else:
                if len(post['san_desc']) != 0:
                    distro = self.get_topic_distribution(post['san_desc'])
                else:
                    continue
                db_tools.addDist2Post(post['_id'], distro)
        
class PostCorpus(object):
    '''gensim corpus to generate lda models'''
    
    def __init__(self,dictionary,data_selector={}):
        self._data_select=data_selector;
        self.dictionary=dictionary
    
    
    def __iter__(self):
        """
        The function that defines a corpus -- iterating over the corpus yields
        bag-of-words vectors, one for each document.
        
        A bag-of-words vector is simply a list of ``(tokenId, tokenCount)`` 2-tuples.
        """        
        posts = db_tools.get_posts().find(self._data_select)
        for post in posts:
            if len(post['san_full']) != 0:
                yield self.dictionary.lda_dict().doc2bow(post['san_full'])
            else:
                if len(post['san_desc']) != 0:
                    yield self.dictionary.lda_dict().doc2bow(post['san_desc'])
                else:
                    logging.debug('Post has no [san_full] and no [san_desc]'+post['link'] )

    def __getitem__(self,index):
        posts = db_tools.get_posts().find(self._data_select)[index]
        return posts
    def __len__(self):
        return db_tools.get_posts().count()
    


class FeedCorpus(object):
    def __init__(self,dictionary):
        self.dictionary=dictionary
    def __iter__(self):
        feeds = db_tools.get_feeds().find()
        for feed in feeds:
            posts = db_tools.get_posts().find({'feed_id':feed["_id"]})
            text = []
            for post in posts:
                text.extend(post['san_full'])
            yield self.dictionary.lda_dict().doc2bow(text)
            
            
