'''
Created on 15.05.2012

@author: dominik
'''
from setuptools import setup, find_packages
packages = find_packages()
setup(name='feedaro-backend',
      version="0.2",
      install_requires=['feedparser'],
      package_data = {'':['*.ini']},
      packages=packages,
      entry_points={
                    'console_scripts':[
                            'feedaro = feedaro.manage:__main__'
                                       ]}
)