package de.hdm.feedaro;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.document.TextDocument;
import de.l3s.boilerpipe.extractors.ExtractorBase;
import de.l3s.boilerpipe.filters.english.MinFulltextWordsFilter;
import de.l3s.boilerpipe.filters.english.NumWordsRulesClassifier;
import de.l3s.boilerpipe.filters.heuristics.BlockProximityFusion;
import de.l3s.boilerpipe.filters.heuristics.KeepLargestBlockFilter;
import de.l3s.boilerpipe.filters.simple.BoilerplateBlockFilter;
import de.l3s.boilerpipe.filters.simple.SplitParagraphBlocksFilter;

public class FeedaroExtractor extends ExtractorBase{

	MinFulltextWordsFilter minWords = new MinFulltextWordsFilter(5);
	SplitParagraphBlocksFilter spf = new SplitParagraphBlocksFilter();
	BoilerplateBlockFilter bbf = BoilerplateBlockFilter.INSTANCE;
	BlockProximityFusion proxi = new BlockProximityFusion(1, true, false);
	
	public FeedaroExtractor() {
	}
	
	public boolean process(TextDocument doc) throws BoilerpipeProcessingException {
		    NumWordsRulesClassifier.INSTANCE.process(doc);
				   	 minWords.process(doc);
				   	BoilerplateBlockFilter.getInstance().process(doc);
	                boolean  foo = proxi.process(doc);
	                System.out.println(foo);
	                 KeepLargestBlockFilter.INSTANCE.process(doc);
		    return true;
		}
	
	/*
	 * 	@Override
	public boolean process(TextDocument doc)
			throws BoilerpipeProcessingException {
		return
				NumWordsRulesClassifier.INSTANCE.process(doc)|
				MinClauseWordsFilter.INSTANCE.process(doc) |
			
	 */

}
