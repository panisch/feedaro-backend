package de.hdm.feedaro;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.nio.charset.Charset;

public class Newsgrabber {
	private final static int PORT= 6669;
	private ServerSocket serverSocket;
	FeedaroExtractor extractor = new FeedaroExtractor();
	
	
	public static void main(String[] args) throws IOException {
		new Newsgrabber().startServer();
	}
	private String getContent(String url){
		   URL url2;
		try {
			url2 = new URL(url);
			return extractor.getText(url2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	private void startServer() throws IOException{
		serverSocket=new ServerSocket(PORT);
		while(true){
			try{
				System.out.println("wait");
			Socket client = serverSocket.accept();
			System.out.println("handle");
			handleConnection(client);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	private void handleConnection(Socket client) throws IOException{
		InputStream in = client.getInputStream();
		OutputStream out = client.getOutputStream();
		BufferedReader bin = new BufferedReader(new InputStreamReader(in));
		DataOutputStream pout = new DataOutputStream(out);
		String url =bin.readLine();
		System.out.println(url);
		String content = getContent(url);
		byte [] data = content.getBytes(Charset.forName("UTF-8"));
		pout.write(data);
		pout.flush();
		client.close();
	}
}
